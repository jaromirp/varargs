package pl.sda.kalkulator;

import pl.sda.args.DodawanieParametrami;

public class Main {
    public static void main(String[] args) {

        String dzialanie = args[0]; //+, -
        args[0] = "0";


        Kalkulator kalkulator = new Kalkulator();

        int[] liczby = DodawanieParametrami.zamienNaLiczby(args);

        switch (dzialanie) {
            case "+":
                System.out.println(kalkulator.dodaj(liczby));
                break;

            case "-":
                System.out.println(kalkulator.odejmij(liczby));
                break;
        }

    }
}
