package pl.sda.kalkulator;

public class Kalkulator {
    public int dodaj(int... liczby) {

        int suma = 0;

        for (int liczba : liczby) {
            suma += liczba;

        }
        return suma;

    }

    public int odejmij(int... liczby) {

        int suma = liczby[1];

        for (int i=2;i<liczby.length;i++) {
            suma -= liczby[i];

        }
        return suma;

    }

}
