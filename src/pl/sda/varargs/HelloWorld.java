package pl.sda.varargs;

public class HelloWorld {

    public static void main(String[] args) {

        hello("Ania", "Jacek", "Krzysiek");

        //String.format()  tu leci tablica gdzie można wydrukować teskt

    }

    public static void hello (String... imiona) {
        for (String imie:imiona) {
            System.out.println("Cześć" + imie);
        }
    }
}
